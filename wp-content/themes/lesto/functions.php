<?php
/**
 * Lesto functions and definitions
 *
 * @package Lesto
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'lesto_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function lesto_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Lesto, use a find and replace
	 * to change 'lesto' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'lesto', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'lesto' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'lesto_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // lesto_setup
add_action( 'after_setup_theme', 'lesto_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function lesto_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'lesto' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'lesto_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function lesto_scripts() {

	// CSS
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style( 'photoswipe-css', get_template_directory_uri() . '/css/magnific-popup.css');
	wp_enqueue_style( 'lesto', get_stylesheet_uri() );
	wp_enqueue_style( 'ilight-css', get_template_directory_uri() . '/css/ilightbox.css');
	wp_enqueue_style( 'slicknav-css', get_template_directory_uri() . '/css/slicknav.css');


	// JS
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true);
	wp_enqueue_script( 'ilight-js', get_template_directory_uri() 	. '/js/ilightbox.js', array('jquery'), '1.0', true);
	wp_enqueue_script( 'slicknav-js', get_template_directory_uri() 	. '/js/jquery.slicknav.min.js', array('jquery'), '1.0', true);
	wp_enqueue_script( 'scrollto-js', get_template_directory_uri() 	. '/js/jquery-scrollto.js', array('jquery'), '1.0', true);
    wp_enqueue_script( 'magnific-js', get_template_directory_uri()     . '/js/jquery.magnific-popup.min.js', array('jquery'), '1.0', true);
	wp_enqueue_script( 'geral-js', get_template_directory_uri() 	. '/js/geral.js', array('jquery'), '1.3', true);

	//wp_enqueue_script( 'lesto-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	//wp_enqueue_script( 'lesto-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'lesto_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// CONFGURAÇÕES VIA REDUX
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}

//Making jQuery Google API
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', false, '1.8.1');
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'modify_jquery');

function  strip_shortcode_gallery( $content ) {
    preg_match_all( '/'. get_shortcode_regex() .'/s', $content, $matches, PREG_SET_ORDER );
    if ( ! empty( $matches ) ) {
        foreach ( $matches as $shortcode ) {
            if ( 'gallery' === $shortcode[2] ) {
                $pos = strpos( $content, $shortcode[0] );
                if ($pos !== false)
                    return substr_replace( $content, '', $pos, strlen($shortcode[0]) );
            }
        }
    }
    return $content;
}

function set_html_content_type() { return 'text/html'; }

function my_wp_default_styles($styles)
{
	$styles->default_version = "20150309";
}
add_action("wp_default_styles", "my_wp_default_styles");

function formularioContato(){

    $nome       = $_POST['contatoNome'];
    $email      = $_POST['contatoEmail'];
    $telefone   = $_POST['contatoTelefone'];
    $unidade    = $_POST['contatoUnidade'];
    $mensagem   = $_POST['contatoMensagem'];


    $html  = '<p>Dados do interessado no corpo do e-mail:</p>';
    $html .= '<hr />';
    $html .= '<b>Nome</b>: '        . $nome       . '<br />';
    $html .= '<b>E-mail</b>: '      . $email      . '<br />';
    $html .= '<b>Telefone</b>: '    . $telefone   . '<br />';
    $html .= '<b>Unidade</b>: '     . $unidade   . '<br />';
    $html .= '<b>Mensagem</b>: '    . $mensagem   . '<br />';

    add_filter( 'wp_mail_content_type', 'set_html_content_type' );

    $envio = wp_mail('contato@lesto.com.br', 'Contato através do site', $html);

   	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	die();
}
add_action('wp_ajax_formularioContato', 'formularioContato');
add_action('wp_ajax_nopriv_formularioContato', 'formularioContato');

function formularioVisita(){

    $nome       = $_POST['visitaNome'];
    $email      = $_POST['visitaEmail'];
    $telefone   = $_POST['visitaTelefone'];
    $data   	= $_POST['visitaData'];
    $horario   	= $_POST['visitaHorario'];
    $unidade    = $_POST['visitaUnidade'];

    var_dump($unidade);
    $html  = '<p>Dados cadastrados:</p>';
    $html .= '<hr />';
    $html .= '<b>Nome</b>: '        . $nome       . '<br />';
    $html .= '<b>E-mail</b>: '      . $email      . '<br />';
    $html .= '<b>Telefone</b>: '    . $telefone   . '<br />';
    $html .= '<b>Data</b>: '    	. $data   	  . '<br />';
    $html .= '<b>Hora</b>: '    	. $horario    . '<br />';
    $html .= '<b>Unidade</b>: '     . $unidade    . '<br />';

    add_filter( 'wp_mail_content_type', 'set_html_content_type' );

    $envio = wp_mail('contato@lesto.com.br', 'Agendamento de visita através do site', $html);

   	remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	die();
}
add_action('wp_ajax_formularioVisita', 'formularioVisita');
add_action('wp_ajax_nopriv_formularioVisita', 'formularioVisita');

add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    //$output = "<div class=\"slideshow-wrapper\">\n";
    //$output .= "<div class=\"preloader\"></div>\n";
    $output = "<ul>\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img 	= wp_get_attachment_image_src($id, 'full');
        $thumb 	= wp_get_attachment_image_src($id, 'thumbnail');

        $output .= "<li>\n";
        $output .= "<a href=\"{$img[0]}\" class=\"galeria\">\n";
        $output .= "<img src=\"{$thumb[0]}\" width=\"{$thumb[1]}\" height=\"{$thumb[2]}\" alt=\"\" />\n";
        $output .= "</a>";
        $output .= "</li>\n";
    }

    $output .= "</ul>\n";
   // $output .= "</div>\n";

    return $output;
}


 // VERSIONAMENTO DE FOLHAS DE ESTILO
 function versionamentoEstilos($estilos){
 $estilos->default_version = "01092017";
 }
 add_action("wp_default_styles", "versionamentoEstilos");