<?php
/**
 * Template Name: Página inicial
 * Description:
 *
 * @package lesto
 */

get_header();

?>

	<!-- PÁGINA INICIAL -->
	<div class="pg-inicial">

		<!-- CARROSSEL -->
		<div class="row carrossel">

			<div class="col-md-12">

				<!-- SLIDER -->
				<?php
					$d = 1;
		        	$destaques = new WP_Query( array( 'post_type' => 'destaques', 'posts_per_page' => -1 ) );
					while ( $destaques->have_posts() ) : $destaques->the_post();
					$bg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$ativa 		= ($d == 1) ? 'ativo' : '';
					$mostrar 	= ($d == 1) ? '' : 'display: none;';
				?>
				<div class="row carrossel-item <?php echo $ativa; ?>" id="ci-<?php echo $d; ?>" style="background-image: url(<?php echo $bg[0]; ?>); <?php echo $mostrar; ?>">
					<div class="container">
						<?php

							$img1 = rwmb_meta('Lesto_destaque_imagem1','type=image');
							foreach ($img1 as $img) {
							 	echo '<img src="' . $img['full_url'] . '" class="img1 hidden-xs hidden-sm"/>';
							}

							$img2 = rwmb_meta('Lesto_destaque_imagem2','type=image');
							foreach ($img2 as $img) {
							 	echo '<img src="' . $img['full_url'] . '" class="img2 hidden-xs hidden-sm"/>';
							 }

						?>
						<div class="carrossel-titulo">
							<h3 class="hidden-xs hidden-sm"><?php echo get_the_title(); ?></h3>
							<h3 class="hidden-md hidden-lg" style="font-size: 1.6em;"><?php echo get_the_title(); ?></h3>
						</div>
					</div>
				</div>
				<?php
					if($d == 1){
						$htmlIndicadores = '<li class="cind ativo" id="cind-1">1</li>';
					}else {
						$htmlIndicadores .= '<li class="cind" id="cind-' . $d . '">' . $d . '</li>';
					}
				?>
				<?php $d++; endwhile; wp_reset_query(); ?>

				<div class="container">
					<ul class="carrossel-indicadores list-inline hidden-xs hidden-sm">
						<?php echo $htmlIndicadores; ?>
					</ul>
				</div>

			</div>

		  <script>

		  
		  		var giraCarrossel = setInterval(function(){

		  			var carrosselItemId = $('.carrossel-item.ativo').attr('id');
		  				carrosselItemId = carrosselItemId.split('-');
		  				carrosselItemId = carrosselItemId[1];

		  			var carrosselItemIdProximo = parseInt(carrosselItemId) + 1;

		  			if(carrosselItemIdProximo == <?php echo $d; ?>){
		  				carrosselItemIdProximo = 1;
		  			}

		  			$('.carrossel-item').hide();
		  			$('.carrossel-item').removeClass('ativo');
		  			$('#ci-' + carrosselItemIdProximo).fadeIn(1500);
		  			$('#ci-' + carrosselItemIdProximo).addClass('ativo');

		  			$('.carrossel-indicadores li').removeClass('ativo');
		  			$('#cind-' + carrosselItemIdProximo).addClass('ativo');

		  			$('.carrossel-indicadores li').click(function(e){
		  				var cindId = $(this).attr('id');
		  				cindId = cindId.split('-');
		  				cindId = cindId[1];

		  				clearInterval(giraCarrossel);
		  				$('.carrossel-indicadores li').removeClass('ativo');
		  				$(this).addClass('ativo');
		  				$('.carrossel-item').hide();
		  				$('#ci-' + cindId).fadeIn();
		  			});

		  		},8000);





		  </script>

		</div>

		<div class="container pagina">

			<!-- SERVIÇOS -->
			<div class="row servicos-tipos">

				<?php
					$d = 1;
		        	$destaques = new WP_Query( array( 'post_type' => 'destaques', 'posts_per_page' => -1 ) );
					while ( $destaques->have_posts() ) : $destaques->the_post();
					$bg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				?>

				<div class="col-md-3 servico-tipo" id="st-<?php echo $d; ?>">
					<?php
						$miniatura = rwmb_meta('Lesto_destaque_miniatura','type=image');
						foreach ($miniatura as $m) {
						 	echo '<img src="' . $m['full_url'] . '" />';
						}
						$ativa = ($d == 1) ? 'ativa' : '';
					?>

					<div class="btn-aba <?php echo $ativa; ?>">
						<h2><?php echo rwmb_meta('Lesto_destaque_item'); ?></h2>
					</div>


				</div>
				<div class="servicos-descricao descricaoMobile">
					<div class=" servico-descricao">

						<div class="col-md-9">
							<p><?php echo get_the_content(); ?></p>
						</div>

						<div class="col-md-3 text-center">
							<?php if (rwmb_meta('Lesto_destaque_valor')!= "") { ?>
							<b><small style="display: block; font-size: 0.4em;">a partir de </small><?php echo rwmb_meta('Lesto_destaque_valor'); ?><span><?php echo rwmb_meta('Lesto_destaque_valor_frequencia'); ?></span></b>
							<a href="<?php echo rwmb_meta('Lesto_destaque_link'); ?>" class="btn primary">Saiba mais »</a>
							<?php } ?>
						</div>


					</div>
				</div>

				<?php $d++; endwhile; wp_reset_query(); ?>

			</div>

			<!-- DESCRIÇÃO DOS SERVIÇOS -->
			<div class="row servicos-descricao hideen-sm descricaoDesktop">


				<?php
					$d = 1;
		        	$destaques = new WP_Query( array( 'post_type' => 'destaques', 'posts_per_page' => -1 ) );
					while ( $destaques->have_posts() ) : $destaques->the_post();
					$bg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$ativa = ($d == 1) ? '' : 'display: none;';
				?>
				<div class="servico-<?php echo $d; ?> servico-descricao" id="sd-<?php echo $d; ?>" style="<?php echo $ativa; ?>">

					<div class="col-md-9">
						<p><?php echo get_the_content(); ?></p>
					</div>

					<div class="col-md-3">
						<?php if (rwmb_meta('Lesto_destaque_valor')!= "") { ?>
						<b><small style="display: block; font-size: 0.4em;">a partir de </small><?php echo rwmb_meta('Lesto_destaque_valor'); ?><span><?php echo rwmb_meta('Lesto_destaque_valor_frequencia'); ?></span></b>
						<a href="<?php echo rwmb_meta('Lesto_destaque_link'); ?>" class="btn primary">Saiba mais »</a>
						<?php } ?>
					</div>


				</div>
				<?php $d++; endwhile; wp_reset_query(); ?>

			</div>

			<!-- ESTRUTRA & VISITA -->
			<div class="row estrutura-visita">

				<!-- ESTRUTURA -->
				<div class="col-md-6">

					<div class="estrutura-fotos">
						<a href="<?php echo home_url('/estrutura'); ?>" title="Clique para saber mais sobre nossa estrutura"><img src="<?php echo get_template_directory_uri(); ?>/img/conheca.png" style="width: 100%;" /></a>
					</div>

				</div>

				<!-- VISITA -->
				<div class="col-md-6">

					<!-- FORMULÁRIO VISITA -->
					<div class="formulario-visita">
						<p>Faça-nos uma <span>visita</span>!</p>
						<div class="contato-formas"> <p>ou se preferir</p> <span class="contato-email">contato@lesto.com.br</span> 

							<span class="contato-telefone"><i class="fa fa-phone" aria-hidden="true"></i> (41) 3122-2000</span> 
							<span class="contato-telefone"><i class="fa fa-phone" aria-hidden="true"></i> (41) 98725-8970 – Unidade Trajano</span> 
							<span class="contato-telefone"><i class="fa fa-whatsapp" aria-hidden="true"></i>  (41) 99533-8368 - Unidade Neo</span></div>
						
						<div class="formulario">
						<div class="alert alert-success" style="display: none;">
							<span style="font-size: 1.1em;">Sua visita foi agendada com sucesso. Aguardamos você!</span>
						</div>
							<label class="naoalinhado">Nome
								<input type="text" id="visitaNome">
							</label>
							<label class="alinhado clear">E-mail
								<input type="email" id="visitaEmail">
							</label>
							<label class="alinhado">Telefone
								<input type="text" id="visitaTelefone">
							</label>
							<label class="alinhado clear">Data
								<input type="text" id="visitaData">
							</label>
							<label class="alinhado">Horário
								<select id="visitaHorario">
									<option value="">Selecione...</option>
	                                <option value="09:00">09:00</option>
	                                <option value="10:00">10:00</option>
	                                <option value="11:00">11:00</option>
	                                <option value="12:00">12:00</option>
	                                <option value="13:00">13:00</option>
	                                <option value="14:00">14:00</option>
	                                <option value="15:00">15:00</option>
	                                <option value="16:00">16:00</option>
	                                <option value="17:00">17:00</option>
								</select>
							</label>
							<label class="alinhado">Unidade
								<select id="visitaUnidade">
									<option value="">Selecione...</option>
	                                <option value="Unidade Centro Cívico Neo Bussiness">Unidade Centro Cívico Neo Bussiness</option>
	                                <option value="Unidade Trajano Reis, 472">Unidade Trajano Reis, 472</option>
								</select>
							</label>
							<button class="btn btn-verde pull-right clear" id="agendar-visita">Agendar visita</button>
							<div class="clear"></div>
						</div>
					</div>

				</div>

			</div>

			<!-- DEPOIMENTOS -->
			<section class="row depoimentos" style="display: none;">

				<header>
					<p>O que dizem nossos clientes</p>
				</header>

				<?php
					$d = 1;
		        	$depoimentos = new WP_Query( array( 'post_type' => 'depoimentos', 'posts_per_page' => 2 ) );
					while ( $depoimentos->have_posts() ) : $depoimentos->the_post();
					$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				?>
				<div class="col-md-6 depoimento">
					<div class="depoimento-conteudo">
						<figure>
						</figure>
						<p><?php echo get_the_content(); ?></p>
						<b><?php echo get_the_title(); ?><span> - Cliente Lesto Escritórios</span></b>
						<div class="clear"></div>
					</div>
				</div>
				<?php $d++; endwhile; wp_reset_query(); ?>

			</section>

			<section class="row enderecos-comercial">
				<header>
					<p>A primeira impressão da sua empresa</p>
				</header>

				<div class="col-md-6 enderecos">
					<a href="<?php echo home_url('/salas#neo'); ?>" id="linkNeo" title="Unidade Neo Business">
						<div class="imagem-endereco" style="background: url(<?php echo get_template_directory_uri(); ?>/img/neo.png)">
							<div class="nome-endereco">
								<h2>Edifício de alto padrão</h2>
							</div>
						</div>
					</a>

				</div>

				<div class="col-md-6 enderecos">
					<a href="<?php echo home_url('/salas#trajano'); ?>"id="linkTrajano"  title="Unidade Trajano Reis">
						<div class="imagem-endereco" style="background: url(<?php echo get_template_directory_uri(); ?>/img/g52.png)">
							<div class="nome-endereco">
								<h2>Sala de reunião aconchegante</h2>
							</div>
						</div>
					</a>

				</div>

			</section>

			<!-- DIVULGAÇÃO
			<section class="row divulgacoes">

				<div class="col-md-12 divulgacao">
					<div class="mensagem"><p><strong>Personalize</strong> a imagem do seu negócio.</p></div>
				</div>

			</section> -->

			<!-- NOTÍCIAS -->
			<div class="row noticias" style="display: none;">

				<div class="col-md-12">
					<section class="ultimas-noticias">

						<header>
							<span class="text-uppercase">Últimas dicas</span>
						</header>

						<ul>
							<?php
					        	$dicas = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5 ) );
								while ( $dicas->have_posts() ) : $dicas->the_post();
								$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							?>
							<li>
								<span><?php echo  mysql2date('d', $post->post_date); ?>/<?php echo  mysql2date('m', $post->post_date); ?></span>
								<img src="<?php echo $foto[0]; ?>" />
								<a href="<?php the_permalink(); ?>" title="Ver dica completa"><?php echo substr(get_the_title(),0,40); ?>...</a>
							</li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>
					</section>
				</div>

			</div>

		</div>

	</div>

<?php get_footer(); ?>