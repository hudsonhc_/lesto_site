<?php

/**

 * Template Name: Localização

 * Description:

 *

 * @package lesto

 */



get_header();



?>



	<!-- LOCALIZAÇÃO -->

	<div class="pg-localizacao internas container">

		<h3>Localização</h3>

		<div class="row interna-conteudo">

			<div class="col-md-12">



				<p class="pagina-descricao text-center">Venha nos fazer uma visita! Nosso horário de atendimento é de <b>De Seg.  a Sex. das 08:00 as 12:00 e das 13:30 as 18:00</b></p>



				<!-- <div class="row">

					<div class="col-md-6">



						<div class="mapa-localizacao">

							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3603.4555483369345!2d-49.2742439!3d-25.4230292!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40fe0bd134d%3A0x49ed51bbef2af7d3!2sR.+Trajano+R%C3%A9is%2C+472+-+S%C3%A3o+Francisco%2C+Matriz%2C+Curitiba+-+PR%2C+80510-220!5e0!3m2!1spt-BR!2sbr!4v1422843010801" width="530" height="530" frameborder="0" style="border:0"></iframe>

						</div>



					</div>

					<div class="col-md-6 text-center">

						<div class="endereco">

							<p>Rua Trajano Reis, 472</p>

							<span>São Francisco</span>

							<span>Curitiba/PR – Brasil</span>

						</div>

						<img src="<?php echo get_template_directory_uri(); ?>/img/ico_casa.png" style="width: 100%; max-width: 510px;" />

					</div>

				</div> -->

				<section class="row enderecos-comercial">

					<!-- <header>

						<p>Endereço comercial de prestígio</p>

					</header> -->



					<div class="col-md-6 enderecos">

						<h2>Unidade Centro Cívico Neo Business</h2>

						<a href="<?php echo home_url('/salas#neo'); ?>" id="linkNeo"  title="Unidade Neo Business">

							<div class="imagem-endereco" style="background: url(<?php echo get_template_directory_uri(); ?>/img/neo.png)">

								<div class="nome-endereco">

								<p>Avenida Candido de Abreu, 470 – 14º. Andar – Sala 1407</p>

								</div>

							</div>

							<span class="observacao">Entrada de pedestre pela Rua Heitor Stockler de França, 396.</span>

						</a>



					</div>



					<div class="col-md-6 enderecos">

						<h2>Unidade Trajano Reis</h2>

						<a href="<?php echo home_url('/salas#trajano'); ?>" id="linkTrajano"  title="Unidade Neo Business">

							<div class="imagem-endereco" style="background: url(<?php echo get_template_directory_uri(); ?>/img/lestoCasa.jpg)">

								<div class="nome-endereco">

								<p>Rua Trajano Reis, 472</p>

								</div>

							</div>

						</a>



					</div>



				</section>

			</div>

		</div>

	</div>

<?php get_footer(); ?>