<?php
/**
 * Template Name: Salas
 * Description:
 *
 * @package lesto
 */

get_header();

?>

	<!-- CONTATO -->
	<div class="pg-salas internas container">
		<h3 id="trajano">Salas da unidade Trajano Reis, 472</h3>
		<div class="row interna-conteudo">
			<div class="col-md-12">
				<table class="rwd-table">
					<tr>
						<th>Descrição da Sala</th>
						<th>Valor por hora para cliente interno</th>
						<th>Valor por hora para cliente externo</th>
					</tr>
					
					<tr>
						<td data-th="Descrição da Sala">Sala de atendimento para 03 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto08G.jpg" title="Sala de atendimento para 03 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto8.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>
						</td>
						<td data-th="Valor para cliente interno">R$ 14,00/h</td>
						<td data-th="Valor para cliente externo">R$ 20,00/h</td>
					</tr>					

					<tr>
						<td data-th="Descrição da Sala">Sala de atendimento para 03 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto06G.jpg" title="Sala de atendimento para 03 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto6R.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 14,00/h</td>
						<td data-th="Valor para cliente externo">R$ 20,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de atendimento para 03 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto09G.jpg" title="Sala de atendimento para 03 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto9.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						

						<td data-th="Valor para cliente interno">R$ 17,50/h</td>
						<td data-th="Valor para cliente externo">R$ 25,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 05 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto07G.jpg" title="Sala de reunião para 05 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto7.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 17,50/h</td>
						<td data-th="Valor para cliente externo">R$ 25,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 06 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto05G.jpg" title="Sala de reunião para 06 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto66.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 24,00/h</td>
						<td data-th="Valor para cliente externo">R$ 35,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 09 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/Foto01G.jpg" title="Sala de reunião para 08 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/foto01.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 28,00/h</td>
						<td data-th="Valor para cliente externo">R$ 40,00/h</td>
					</tr>

				</table>
				<p>Obs. (1) – Em todas as salas são oferecidos como cortesia, secretária para recepcionar o cliente até a sala, água, café e internet wireless.<br>(2) – Estacionamento próprio de cortesia, para todos, durante o período da reunião, limitado a 15 vagas rotativas.<br>(3) – Exclusivamente nas salas de reunião, são oferecidos como cortesia, tela de LED, água e café expresso diretamente na sala;<br>(4) – Todas as salas possuem AR Condicionado, exceto a primeira sala de atendimento e a sala de reunião para 05 pessoas.</p>
			</div>
		</div>

		<h3 id="neo">Salas da unidade Centro Civico Neo Business</h3>
		<div class="row interna-conteudo">
			<div class="col-md-12">
				<table class="rwd-table">
					<tr>
						<th>Descrição da Sala</th>
						<th>Valor para cliente interno</th>
						<th>Valor para cliente externo</th>
					</tr>
					<tr>
						<td data-th="Descrição da Sala">Sala de atendimento para 03 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/IMG-20170316-WA0014.jpg" title="Sala de atendimento para 03 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/(Circulo)IMG-20170316-WA0014.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 21,00/h</td>
						<td data-th="Valor para cliente externo">R$ 30,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 07 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/IMG-20170316-WA0006.jpg" title="Sala de reunião para 07 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/(Circulo)IMG-20170316-WA0006.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 28,00/h</td>
						<td data-th="Valor para cliente externo">R$ 40,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 07 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/IMG-20170316-WA0004.jpg" title="Sala de reunião para 07 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/(Circulo)IMG-20170316-WA0004.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">R$ 28,00/h</td>
						<td data-th="Valor para cliente externo">R$ 40,00/h</td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 08 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/g18.png" title="Sala de reunião para 08 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/18.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">Valor sob consulta</td>
						<td data-th="Valor para cliente externo"><!-- R$ 50,00/h --></td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 10 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/g32.png" title="Sala de reunião para 10 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/32.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">Valor sob consulta</td>
						<td data-th="Valor para cliente externo"><!-- R$ 55,00/h --></td>
					</tr>

					<tr>
						<td data-th="Descrição da Sala">Sala de reunião para 12 pessoas
							<div class="zoom-gallery">
								<a href="<?php echo get_template_directory_uri(); ?>/img/g52.png" title="Sala de reunião para 12 pessoas" style="width:193px;height:125px;">
									<img style="max-width: 190px" src="<?php echo get_template_directory_uri(); ?>/img/52.png" alt="sala de atentimento para 03 pessoas.">
								</a>
							</div>

						</td>
						<td data-th="Valor para cliente interno">Valor sob consulta</td>
						<td data-th="Valor para cliente externo"><!-- R$ 60,00/h --></td>
					</tr>

				</table>
				<p>Obs. (1) – Em todas as salas são oferecidos como cortesia, secretária para recepcionar o cliente até a sala, água, café, internet e exclusivamente nas salas de reunião, tela de LED.<br>(2) – Todas as salas possuem AR Condicionado <br>(3) As salas de reunião para 08, 10 e 12 pessoas, somente para utilização para períodos de no mínimo 04 horas</p>
			</div>
		</div>
	</div>


<?php get_footer(); ?>

<script>
	$(document).ready(function() {
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}

	});


});
</script>