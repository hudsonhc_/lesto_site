<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Lesto
 */
global $configuracao;
?>

	<!-- RODAPÉ -->
	<footer class="row rodape">

		<div class="col-md-12">

			<!-- MAPA & INFORMAÇÕES DE CONTATO -->
			<div class="row" style="display: none;">
				<div class="col-md-6">
					<div class="rod-1 mapa">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3603.4555483369345!2d-49.2742439!3d-25.4230292!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce40fe0bd134d%3A0x49ed51bbef2af7d3!2sR.+Trajano+R%C3%A9is%2C+472+-+S%C3%A3o+Francisco%2C+Matriz%2C+Curitiba+-+PR%2C+80510-220!5e0!3m2!1spt-BR!2sbr!4v1422843010801" width="100%" height="428" frameborder="0" style="border:0"></iframe>
						<div class="detalhe-mapa hidden-xs hidden-sm">
							<img src="<?php echo get_template_directory_uri(); ?>/img/bg_rodape-mapa.png" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="rod-2">
						<div class="informacoes-contato ">
							<div class="pull-left">
							<span><?php echo $configuracao['opt-endereco']; ?></span>
							<span><?php echo $configuracao['opt-bairro']; ?></span>
							<span>CEP <?php echo $configuracao['opt-cep']; ?></span>
							<span><?php echo $configuracao['opt-cidade']; ?> - <?php echo $configuracao['opt-uf']; ?></span>
							<br />
							<span><?php echo $configuracao['opt-telefone']; ?></span>
							<span><?php echo $configuracao['opt-email']; ?></span>
							</div>
							<img src="<?php echo get_template_directory_uri(); ?>/img/ico_pin2.png" class="pull-right" />
						</div>

						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="rod-1 mapa">
						<!-- <iframe src="https://maps.google.com.br/maps?q=Avenida Candido de Abreu, 470&output=embed" width="100%" height="428" frameborder="0" style="border:0" allowfullscreen></iframe> -->
						<div style="width: 100%; overflow: hidden; height: 428px;">
							<iframe
								src="https://maps.google.com.br/maps?q=Avenida Candido de Abreu, 470&output=embed"
								width="100%"
								height="800"
								frameborder="0"
								style="border:0; margin-top: -150px;">
							</iframe>
						</div>
						<div class="rua">
							<i class=" fa fa-map-marker"></i><span>Av. Cândido de Abreu, 470 14º andar – Sala 1407, Curitiba</span>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="rod-1 mapa">
						<!-- <iframe src="https://maps.google.com.br/maps?q=R. Trajano Reis, 472&output=embed" width="100%" height="428" frameborder="0" style="border:0" allowfullscreen></iframe> -->
						<div style="width: 100%; overflow: hidden; height: 428px;">
							<iframe
								src="https://maps.google.com.br/maps?q=R. Trajano Reis, 472&output=embed"
								width="100%"
								height="800"
								frameborder="0"
								style="border:0; margin-top: -150px;">
							</iframe>
						</div>
						<div class="rua">
							<i class=" fa fa-map-marker"></i><span>R. Trajano Reis, 472 - São Francisco, Curitiba</span>
						</div>
					</div>
				</div>

			</div>

		</div>

	</footer>

	<!-- COPYRIGHT -->
	<div class="row  enderecosRodape copyright">
		<div class="container">
			<div class="col-md-6 text-left">
				<p style="color:#fff;font-size: 18px;text-align:left;" class=""><i class=" fa fa-map-marker"></i><span> Av. Cândido de Abreu, 470 14º andar – Sala 1407, Curitiba</span></p>
			</div>
			<div class="col-md-6 text-right">
					<p style="color:#fff;;font-size: 18px;font-weight: 400;text-align:right;"  class=""><i class=" fa fa-map-marker"></i><span> R. Trajano Reis, 472 - São Francisco, Curitiba</span></p>
			</div>
		</div>
	</div>
	<div class="row copyright">
		<div class="container">
			<div class="col-md-6 text-left">
				<small>© Copyright 2015 Lesto Escritórios - Todos os direitos reservados</small>
			</div>
			<div class="col-md-6 text-right">
				<small class="selo-pixele">
						                <span>Desenvolvido por</span>
	                <a href="http://www.pixele.com.br" target="_blank" title="Desenvolvido por Pixele - be creative"><img src="http://www.pixele.com.br/img/selo_pixele.png"></a>
	            </small>
			</div>
		</div>
	</div>

<?php wp_footer(); ?>

</body>
</html>
