<?php
/**
 * The template for displaying all single posts.
 *
 * @package Lesto
 */

get_header(); ?>

	<!-- DICA -->
	<div class="pg-dicas internas container">
		<h3>Dicas</h3>
		<hr />
		<div class="row interna-conteudo" style="background-color: transparent;">
			<div class="col-md-12">

				<div class="row">
					<div class="col-md-12">

						<?php while ( have_posts() ) : the_post(); 
						  
						  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
						  $url = $thumb['0'];

						?>

							<div class="dica">
								<figure style="background-image: url('<?php echo $url; ?>');">
									<figcaption class="dica-data">
										<span class="dia"><?php echo  mysql2date('d', $post->post_date); ?></span>
										<span class="mes"><?php echo  strtoupper(mysql2date('M', $post->post_date)); ?></span>
									</figcaption>
								</figure>
								<h2><?php echo get_the_title(); ?></h2>
								<?php the_content(); ?>
							</div>

							<?php
								// If comments are open or we have at least one comment, load up the comment template
								if ( comments_open() || get_comments_number() ) :
									comments_template();
								endif;
							?>

						<?php endwhile; ?>
						
					</div>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
