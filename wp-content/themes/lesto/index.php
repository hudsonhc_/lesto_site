<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lesto
 */

get_header(); ?>

	<!-- DICAS -->
	<div class="pg-dicas internas container">
		<h3>Dicas</h3>
		<hr />
		<div class="row interna-conteudo" style="background-color: transparent;">
			<div class="col-md-12">

				<div class="text-center">
					<p class="pagina-descricao text-center">Fique por dentro de algumas dicas que podem favorecer o seu negócio.</b></p>
					<span class="glyphicon glyphicon-chevron-down" style="font-size: 2em;"></span>
					<br /><br />
				</div>

				<div class="row">
					<?php if ( have_posts() ) : ?>

						<?php while ( have_posts() ) : the_post(); ?>

						  <?php
						  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
						  $url = $thumb['0'];
						  ?>

							<div class="col-md-6">
								<div class="dica">
									<figure style="background-image: url('<?php echo $url; ?>');">
										<figcaption class="dica-data">
											<span class="dia"><?php echo  mysql2date('d', $post->post_date); ?></span>
											<span class="mes"><?php echo  strtoupper(mysql2date('M', $post->post_date)); ?></span>
										</figcaption>
									</figure>
									<h2><?php echo get_the_title(); ?></h2>
									<p><?php echo get_the_excerpt(); ?></p>
									<a href="<?php the_permalink(); ?>" class="dica-link pull-right">Ler mais</a>
									<div class="clear"></div>
								</div>
							</div>

						<?php endwhile; ?>

						<?php the_posts_navigation(); ?>

					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
