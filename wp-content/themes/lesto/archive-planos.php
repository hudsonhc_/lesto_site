<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lesto
 */

get_header(); ?>

	<!-- PLANOS -->
	<div class="pg-planos internas container">

		

		<div class="row interna-conteudo">
			<div class="col-md-12">

			<?php if ( have_posts() ) : ?>

				<?php
				$i = 1;
				while ( have_posts() ) : the_post();
				$zebra = ($i%2 == 0) ? 'plano-b' : 'plano-a';
				?>

					<div class="row plano <?php echo $zebra; ?>">
						<div class="col-md-2 plano-icone">

						<?php
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
							$url = $thumb['0'];
						?>
							<img src="<?php echo $url; ?>" width="150" />
						</div>
						<div class="col-md-6 plano-vantagens">
							<h2><?php echo get_the_title(); ?></h2>
							<ul>
								<?php
								foreach (rwmb_meta('Lesto_plano_items') as $item) {
									echo '<li>' . $item . '</li>';
								}
								?>
							</ul>
							<br />
							<!-- <?php if(is_array(rwmb_meta('Lesto_plano_items2'))): ?>
							<p style="font-size: 1.4em;"><?php echo rwmb_meta('Lesto_plano_titulo_items2'); ?></p>
							<ul>
								<?php
								foreach (rwmb_meta('Lesto_plano_items2') as $item) {
									echo '<li style="font-size: 1.4em;">' . $item . '</li>';
								}
								?>
							</ul>
							<?php endif; ?> -->
							<?php if(is_array(rwmb_meta('Lesto_plano_titulo_items2'))): ?>
								<?php
									$arrayTitulos = rwmb_meta('Lesto_plano_titulo_items2');

									foreach ($arrayTitulos as $arrayTitulo) {

								?>
									<p style="font-size: 1.4em;"><?php echo $arrayTitulo; ?></p>

								<?php } ?>
							<?php endif; ?>
						</div>
						<div class="col-md-4 plano-descricao">
							<b><?php echo rwmb_meta('Lesto_plano_valor'); ?><span><?php echo rwmb_meta('Lesto_plano_frequencia'); ?></span></b>
							<p style=""><?php echo  get_the_content(); ?></p>
						</div>
					</div>

				<?php $i++; endwhile; ?>

				<?php the_posts_navigation(); ?>

			<?php else : ?>


			<?php endif; ?>

			<br />
			<div class="btn-faleconosco text-center">
				<span>Para contratar nossos serviços, <a href="<?php echo home_url('/contato'); ?>">fale conosco</a>.</span>
			</div>

			</div>

		</div>
		<h3>Os valores dos planos, descrito acima, são para contratos por prazo indeterminado, podendo ser cancelado a qualquer momento, mediante aviso prévio de 30 dias.</h3>

	</div>

<?php get_footer(); ?>
