<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Lesto
 */

get_header(); ?>

	<!-- VANTAGENS OFERECIDAS -->
	<div class="pg-vantagens internas container">
		<h3>Vantagens oferecidas</h3>
		<div class="row interna-conteudo">
			<div class="col-md-12">

				<p class="pagina-descricao text-center">Conheça algumas vantagens de ser um cliente <b>Lesto Escritórios</b></p>

				<br /><br />

				<div class="row">
					<?php if ( have_posts() ) : 
					$v = 1;
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-md-4">
						<div class="vantagem">
							<span class="numero"><?php echo $v; ?></span>
							<div class="selo">
								<p><?php echo  get_the_title(); ?></p>
							</div>
							<p class="descricao"><?php echo  get_the_content(); ?></p>
						</div>
					</div>
					<?php $v++; endwhile; ?>
					<?php endif; ?>
				</div>

				<div class="row text-center">
					<div class="col-md-12">
						<br />
						<div class="btn-faleconosco">
							<span>Para contratar nossos serviços, <a href="<?php echo home_url('/contato'); ?>">fale conosco</a>.</span>
						</div>
					</div>
				</div>

				<br />

			</div>
		</div>
	</div>

<?php get_footer(); ?>
